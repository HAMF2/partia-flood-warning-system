from floodsystem.geo import rivers_with_station, stations_by_river
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1D"""

    # Build list of stations
    stations = build_station_list()

    # Use rivers_with_station to build a list of monitored rivers
    rivers = rivers_with_station(stations)

    # Print number of rivers with stations
    print("There are %d rivers with stations.\n" % len(rivers))

    # Sort rivers and reduce list to first 10
    rivers.sort()
    rivers = rivers [:10]

    # Print list
    print(rivers, "\n")

    ### Task 1D part 2 ###

    # Use stations by river to generate a dictionary of stations by river
    rivers_stations = stations_by_river(stations)

    #print(rivers_stations)

    # Find, sort and print stations for rivers given
    for river in [
        "River Aire", "River Cam", "River Thames"
    ]:
        this_rivers_stations = rivers_stations[river]

        # Reduce list from station objects to names
        for i in range(len(this_rivers_stations)):
            this_rivers_stations[i] = this_rivers_stations[i].name
        
        this_rivers_stations.sort()

        print("Stations on the %s:\n" % river)
        print(this_rivers_stations, "\n")


if __name__ == "__main__":
    print("\n*** Task 1D: CUED Part IA Flood Warning System ***\n")
    run()

