from floodsystem.station import MonitoringStation
from floodsystem.stationdata import update_water_levels
from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level

def run():
    """requirements for task 2C"""
    
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    levels = stations_highest_rel_level(stations, 10)
    
    for station in levels:
        print (station[0].name, station[1])


if __name__ == "__main__":
    print("\n*** Task 2C: CUED Part IA Flood Warning System ***\n")
    run()