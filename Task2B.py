from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import update_water_levels
from floodsystem.stationdata import build_station_list

def run():
    """requirements for task 2B"""
    
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)

    ratios = stations_level_over_threshold(stations, 0.8)
    #print(ratios)
    for station in ratios:
        print (station[0].name, station[1])


if __name__ == "__main__":
    print("\n*** Task 2B: CUED Part IA Flood Warning System ***\n")
    run()