# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation
from floodsystem.station import inconsistent_typical_range_stations

def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_typical_range_consistent():
    # build a consistent river and two inconsistent rivers
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-1.0, 2.0)
    trange = (-2.0, 3.5)
    trange_reverse = (3.5, -2,0)
    river = "Random River "
    town = "Random Town"
    sgood = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    sbad1 = MonitoringStation(s_id, m_id, label, coord, trange_reverse, river, town)
    sbad2 = MonitoringStation(s_id, m_id, label, coord, None, river, town)

    # check that function identifies inconsistent rivers
    assert sgood.typical_range_consistent() == True
    assert sbad1.typical_range_consistent() == False
    assert sbad2.typical_range_consistent() == False

def test_inconsistent_typical_range_stations():
    # build two consistent rivers and two inconsistent rivers
    s_id = "test-s-id"
    m_id = "test-m-id"
    coord = (-1.0, 2.0)
    trange = (-2.0, 3.5)
    trange_reverse = (3.5, -2,0)
    river = "Random River "
    town = "Random Town"
    sgood1 = MonitoringStation(s_id, m_id, "sgood1", coord, trange, river, town)
    sgood2 = MonitoringStation(s_id, m_id, "sgood2", coord, trange, river, town)
    sbad1 = MonitoringStation(s_id, m_id, "sbad1", coord, trange_reverse, river, town)
    sbad2 = MonitoringStation(s_id, m_id, "sbad2", coord, None, river, town)

    # build tuple of stations
    stations = (sgood1, sgood2, sbad1, sbad2)

    # ensure that function identifies and returns a list of inconsistent stations
    assert inconsistent_typical_range_stations(stations) == [sbad1, sbad2]

def test_relative_water_level():
    
    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    s.latest_level = 2
    last = s.relative_water_level()
    assert abs(last - 0.7485420837) < 0.001

    s.latest_level = -2.3 
    assert s.relative_water_level() == 0

    s.latest_level = 3.4445
    assert s.relative_water_level() == 1

    s.latest_level = None
    assert s.relative_water_level() == None
