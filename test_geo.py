"""Unit test for the geo module"""

from floodsystem.geo import stations_by_distance, stations_within_radius, rivers_with_station, stations_by_river, rivers_by_station_number
from floodsystem.station import MonitoringStation
def test_stations_by_distance():

    # Build list of stations
    stations = [MonitoringStation("id_1", "measure_id_1", "label_1", (52.2053, 0.1218), (0.0, 1.0), "river_1", "town_1"),
            MonitoringStation("id_2", "measure_id_2", "label_2", (52.2053, 0.1518), (0.0, 1.0), "river_2", "town_2"),
            MonitoringStation("id_3", "measure_id_3", "label_3", (52.2053, 0.1718), (0.0, 1.0), "river_3", "town_3"),
            MonitoringStation("id_4", "measure_id_4", "label_4", (52.2053, 1.1218), (0.0, 1.0), "river_4", "town_4"),
            MonitoringStation("id_5", "measure_id_5", "label_5", (53.2253, 0.1218), (0.0, 1.0), "river_5", "town_5")]

    # Coordinate to find distance from
    cam_coord = (52.2053, 0.1218)

    # Sort by distance
    stations_with_distance = stations_by_distance(stations, cam_coord)

    last_dist = 0
    counter = 0

    for station_w_dist in stations_with_distance:
        assert station_w_dist[1] >= last_dist
        last_dist = station_w_dist[1]
        counter += 1
    
    assert counter > 0

def test_stations_within_radius():

    # Build list of stations
    stations = [MonitoringStation("id_1", "measure_id_1", "label_1", (52.2053, 0.1218), (0.0, 1.0), "river_1", "town_1"),
            MonitoringStation("id_2", "measure_id_2", "label_2", (52.2053, 0.1518), (0.0, 1.0), "river_1", "town_1"),
            MonitoringStation("id_3", "measure_id_3", "label_3", (52.2053, 0.1718), (0.0, 1.0), "river_2", "town_1"),
            MonitoringStation("id_4", "measure_id_4", "label_4", (52.2053, 1.1218), (0.0, 1.0), "river_3", "town_1"),
            MonitoringStation("id_5", "measure_id_5", "label_5", (53.2253, 0.1218), (0.0, 1.0), "river_3", "town_1")]

    # Coordinate to find distance from
    cam_coord = (52.2053, 0.1218)

    # Use stations_within_distance function stations within 10 km
    stations_within_distance = stations_within_radius(stations, cam_coord, 50)

    assert len(stations_within_distance) == 3
    for station in stations_within_distance:
        assert station.name in ["label_1", "label_2", "label_3"]

def test_rivers_with_station():

    # Build list of stations
    stations = [MonitoringStation("id_1", "measure_id_1", "label_1", (52.2053, 0.1218), (0.0, 1.0), "river_1", "town_1"),
            MonitoringStation("id_2", "measure_id_2", "label_2", (52.2053, 0.1518), (0.0, 1.0), "river_1", "town_1"),
            MonitoringStation("id_3", "measure_id_3", "label_3", (52.2053, 0.1718), (0.0, 1.0), "river_2", "town_1"),
            MonitoringStation("id_4", "measure_id_4", "label_4", (52.2053, 1.1218), (0.0, 1.0), "river_3", "town_1"),
            MonitoringStation("id_5", "measure_id_5", "label_5", (53.2253, 0.1218), (0.0, 1.0), "river_3", "town_1")]

    # Use rivers_with_station to build a list of monitored rivers
    rivers = rivers_with_station(stations)

    assert len(rivers) == 3

    for river in ["river_1", "river_2", "river_3"]:
        assert river in rivers

def test_stations_by_river():

    # Build list of stations
    stations = [MonitoringStation("id_1", "measure_id_1", "label_1", (52.2053, 0.1218), (0.0, 1.0), "river_1", "town_1"),
            MonitoringStation("id_2", "measure_id_2", "label_2", (52.2053, 0.1518), (0.0, 1.0), "river_1", "town_1"),
            MonitoringStation("id_3", "measure_id_3", "label_3", (52.2053, 0.1718), (0.0, 1.0), "river_2", "town_1"),
            MonitoringStation("id_4", "measure_id_4", "label_4", (52.2053, 1.1218), (0.0, 1.0), "river_3", "town_1"),
            MonitoringStation("id_5", "measure_id_5", "label_5", (53.2253, 0.1218), (0.0, 1.0), "river_3", "town_1")]

    # Use stations by river to generate a dictionary of stations by river
    rivers_stations = stations_by_river(stations)

    # Use rivers_with_station to build a list of monitored rivers
    rivers = rivers_with_station(stations)

    assert len(rivers_stations) > 0

    for river in rivers:
        assert len(rivers_stations[river]) > 0
        for station in rivers_stations[river]:
            assert station.river == river

def test_rivers_by_station_number():
    #Build list of stations
    stations = [MonitoringStation("id_1", "measure_id_1", "label_1", (0.0, 0.0), (0.0, 1.0), "river_1", "town_1"),
                MonitoringStation("id_2", "measure_id_2", "label_2", (0.0, 0.0), (0.0, 1.0), "river_1", "town_1"),
                MonitoringStation("id_3", "measure_id_3", "label_3", (0.0, 0.0), (0.0, 1.0), "river_2", "town_1"),
                MonitoringStation("id_4", "measure_id_4", "label_4", (0.0, 0.0), (0.0, 1.0), "river_3", "town_1"),
                MonitoringStation("id_5", "measure_id_5", "label_5", (0.0, 0.0), (0.0, 1.0), "river_3", "town_1")]

    
    # Use river_by_station_number function within geo for 1 station
    number_stations = rivers_by_station_number(stations, 1)
    
    # Check that correct result is given
    assert(number_stations == [("river_1", 2), ("river_3", 2)])

#if __name__ == "__main__":
#    test_stations_by_distance()
#    test_stations_within_radius()
#    test_rivers_with_station()
#    test_stations_by_river()