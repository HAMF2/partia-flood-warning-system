import floodsystem.geo as geo
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1E"""

    # Build list of stations
    stations = build_station_list()

    # Use river_by_station_number function within geo for 9 stations
    number_stations = geo.rivers_by_station_number(stations, 9)
    
    #print list
    print(number_stations)

if __name__ == "__main__":
    print("\n*** Task 1E: CUED Part IA Flood Warning System ***\n")
    run()