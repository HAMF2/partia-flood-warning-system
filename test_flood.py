"""Unit test for the flood module"""

from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold, stations_highest_rel_level

def test_stations_level_over_threshold():
    # Create 5 stations, some with inconsistent data
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange1 = (-2.3, 3.4445)
    trange2 = (-2.3, 1)
    trange3 = (-2.3, -3.4445)
    trange4 = (-2.3, 1)
    trange5 = (-2.3, 1)
    river = "River X"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange1, river, town)
    s2 = MonitoringStation(s_id, m_id, label, coord, trange2, river, town)
    s3 = MonitoringStation(s_id, m_id, label, coord, trange3, river, town)
    s4 = MonitoringStation(s_id, m_id, label, coord, trange4, river, town)
    s5 = MonitoringStation(s_id, m_id, label, coord, trange5, river, town)

    s1.latest_level = 3
    s2.latest_level = 2
    s3.latest_level = 3
    s4.latest_level = 4
    s5.latest_level = 1.5

    stations = (s1, s2, s3, s4, s5)
    assert stations_level_over_threshold(stations, 1) == [(s4, 6.3/3.3), (s2, 4.3/3.3), (s5, 3.8/3.3)]
    assert stations_level_over_threshold(stations, 0) == [(s4, 6.3/3.3), (s2, 4.3/3.3), (s5, 3.8/3.3), (s1, 5.3/5.7445)]

def test_stations_highest_rel_level():
    
    # Create 5 stations, some with inconsistent data
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange1 = (-2.3, 3.4445)
    trange2 = (-2.3, 1)
    trange3 = (-2.3, -3.4445)
    trange4 = (-2.3, 1)
    trange5 = (-2.3, 1)
    river = "River X"
    town = "My Town"
    s1 = MonitoringStation(s_id, m_id, label, coord, trange1, river, town)
    s2 = MonitoringStation(s_id, m_id, label, coord, trange2, river, town)
    s3 = MonitoringStation(s_id, m_id, label, coord, trange3, river, town)
    s4 = MonitoringStation(s_id, m_id, label, coord, trange4, river, town)
    s5 = MonitoringStation(s_id, m_id, label, coord, trange5, river, town)

    s1.latest_level = 3
    s2.latest_level = 2
    s3.latest_level = 3
    s4.latest_level = 4
    s5.latest_level = 1.5

    stations = (s1, s2, s3, s4, s5)
    assert stations_highest_rel_level(stations, 3) == [(s4, 6.3/3.3), (s2, 4.3/3.3), (s5, 3.8/3.3)]
    assert stations_highest_rel_level(stations, 2) == [(s4, 6.3/3.3), (s2, 4.3/3.3)]