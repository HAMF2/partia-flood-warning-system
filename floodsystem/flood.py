from floodsystem.station import MonitoringStation
from floodsystem.stationdata import update_water_levels
from floodsystem.utils import sorted_by_key
from floodsystem.stationdata import build_station_list
from floodsystem.analysis import polyfit
from floodsystem.datafetcher import fetch_measure_levels
import datetime

def stations_level_over_threshold(stations, tol):
    """a function that returns a list of tuples, where each tuple holds a station (object) at which the latest relative water level is over the tolerance and the relative water level at the station"""
    
    # make a list of stations with their ratios
    list = []
    for station in stations:
        ratio = MonitoringStation.relative_water_level(station)
        if ratio == None:
            pass
        elif ratio > tol:
            list.append((station, ratio))
        
    # sort the list by descending order of relative level
    ordered_list = sorted_by_key(list, 1, reverse=True)
    return ordered_list

def stations_highest_rel_level(stations, N):
    """a function that returns a list of the N stations at which the water level, relative to the typical range, is highest"""
    
    # make a list of stations with their levels
    list = []
    for station in stations:
        level = MonitoringStation.relative_water_level(station)
        if level == None:
            pass
        else:
            list.append((station, level))
    
    # sort the list by descending order of relative level
    result = sorted_by_key(list, 1, reverse=True)
    final = result[:N]
    return final

def flood_risk(stations):
    "Find risk of flooding at stations passed to function"

    stations_with_risk = []

    update_water_levels(stations)

    for station in stations:

        risk = 0

        if station.latest_level != None and station.typical_range_consistent():

            if station.relative_water_level() > 0.8:

                risk_category, risk = find_risk_for_station(station)

            elif station.relative_water_level() > 0.6:

                risk_category = "Moderate"

            else:

                risk_category = "Low"

        try:
            risk
        except NameError:
            risk = None

        stations_with_risk.append((station, risk, risk_category))

        #print(station.name, risk, risk_category)

    return stations_with_risk

def find_risk_for_station(station):
    "Quantify risk at a high risk station passed"

    print("\n\nStation name: ", station.name)  # debug

    risk = 0

    risk += station.relative_water_level() - 0.8

    dt = 2
    p = 1
    dates, levels = fetch_measure_levels(station.measure_id,
                                            dt=datetime.timedelta(days=dt))

    if len(dates) > 0:
        poly, d = polyfit(dates, levels, p)
    else:
        print("Dates list empty")
        poly = None

    #print(poly)  # debug

    if poly != None:
        if len(poly.c) >= 2:
            gradient = poly.c[-2]
            print("Gradient = ", gradient)
        else:
            print("Polynomial has insufficient coefficients")
            gradient = None
    else:
        print("No polynomial found")
        gradient = None

    if gradient != None:
        risk += gradient

    print("Risk = ", risk)

    risk_category = ""

    if risk != None:
        if risk > 1:
            risk_category = "Severe"
        elif risk > 0.5:
            risk_category = "High"
        else:
            risk_category = "Moderate"
    else:
        risk_category = "Low"

    print("Risk category = ", risk_category)

    return risk_category, risk