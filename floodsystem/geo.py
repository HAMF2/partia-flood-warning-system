# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
from haversine import haversine


def stations_by_distance(stations, p):
    """Build and return a list of all river level monitoring stations
    as tuples with distance from the coordinate 'p' (MonitoringStation, 
    distance). The returned list is sorted by distance.
    """

    # Declare empty list
    stations_with_distance = []

    # Find distance and add station and distance tuple to list for each station
    for station in stations:
        station_and_distance = (station, haversine(p, station.coord))
        stations_with_distance.append(station_and_distance)

    stations_with_distance = sorted_by_key(stations_with_distance, 1)

    return stations_with_distance


def stations_within_radius(stations, centre, r):
    """Build and return a list of all river level monitoring stations
    as tuples within a radius 'r' from the coordinate 'centre'.
    """

    # Declare empty list
    stations_within_distance = []

    # Check distance for each station
    for station in stations:
        station_distance = (haversine(centre, station.coord))

        # Add station if within desired radius
        if station_distance < r:
            stations_within_distance.append(station)

    return stations_within_distance


def rivers_with_station(stations):
    """Build and return a list of all rivers with a monitoring station.
    """

    # Declare empty list
    rivers = []

    # For each station check if river in list already and if not add it
    for station in stations:
        if station.river not in rivers:
            rivers.append(station.river)

    return rivers


def stations_by_river(stations):
    """Build and return a dictionary with rivers as keys and a list of 
    monitoring stations on that river as values.
    """

    # Generate a list of rivers with stations (dict keys)
    rivers = rivers_with_station(stations)

    # Generate a dictionary with rivers as keys
    rivers_stations = {k: [] for k in rivers}

    # Add each station to the value for its river in the dictionary
    for station in stations:
        rivers_stations[station.river].append(station)

    return rivers_stations


def rivers_by_station_number(stations, N):
    river_numbers = {}
    for station in stations:
        if station.river in river_numbers:
            river_numbers[station.river] += 1
        else:
            river_numbers[station.river] = 1
    sorted_rivers = sorted_by_key(river_numbers.items(), 1, reverse=True)
    while sorted_rivers[N - 1][1] == sorted_rivers[N][1]:
        N += 1
    return sorted_rivers[:N]
