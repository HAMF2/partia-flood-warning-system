"""This module provides functions for graphing
various data.

"""

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime, timedelta
from .analysis import polyfit

def plot_water_levels(station, dates, levels):
    """Construct a plot of the water levels at the station passed
    using the water levels and dates passed as parameters.

    """

    if len(dates) != len(levels):
        print("Dates and levels lengths do not match")
        return

    # Plot graph
    plt.plot(dates, levels, "b-", label = "Water level data")
    plt.plot([dates[len(dates)-1], dates[0]], [station.typical_range[0], station.typical_range[0]], "g-", label = "Typical low")
    plt.plot([dates[len(dates)-1], dates[0]], [station.typical_range[1], station.typical_range[1]], "r-", label = "Typical high")
    
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('Date')
    plt.ylabel('Water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)
    plt.legend()

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels

def plot_water_level_with_fit(station, dates, levels, p):

    if len(dates) != len(levels):
        print("Dates and levels lengths do not match")
        return

    if len(dates) > 0:

        poly, d = polyfit(dates, levels, p)

        t = matplotlib.dates.date2num(dates) 

        plt.plot(dates, poly(t-d), "y-", label = "Best fit")
        plt.plot(dates[:len(levels)], levels, "b-", label = "Water level data")
        if station.typical_range != None:
            if len(station.typical_range) == 2:
                plt.plot([dates[len(dates)-1], dates[0]], [station.typical_range[0], station.typical_range[0]], "g-", label = "Typical low")
                plt.plot([dates[len(dates)-1], dates[0]], [station.typical_range[1], station.typical_range[1]], "r-", label = "Typical high")

        # Add axis labels, rotate date labels and add plot title
        plt.xlabel('Date')
        plt.ylabel('Water level (m)')
        plt.ticklabel_format(style='sci', axis='y', scilimits=(-5,5))
        plt.xticks(rotation=45) 
        plt.legend()
        plt.title("Plot of water level and order " + str(p) + " polynomial \nbest fit line against time at " + station.name)

        # Display plot
        plt.tight_layout()  # This makes sure plot does not cut off date labels

    else:
        print("No data to plot")