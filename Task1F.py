from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list


def run():
    """Requirements for Task 1F"""

    # Build list of stations
    stations = build_station_list()

    # Use inconsistent_typical_range_stations function to find inconsistent stations
    inconsistent_stations = inconsistent_typical_range_stations(stations)

    # sort list
    inconsistent_stations = sorted([station.name for station in inconsistent_stations])

    # print list
    print(inconsistent_stations)


if __name__ == "__main__":
    print("\n*** Task 1F: CUED Part IA Flood Warning System ***\n")
    run()
