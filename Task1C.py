from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1C"""

    # Build list of stations
    stations = build_station_list()

    # Coordinate to find distance from
    cam_coord = (52.2053, 0.1218)

    # Use stations_within_distance function stations within 10 km
    stations_within_distance = stations_within_radius(stations, cam_coord, 10)

    # Take required data from MonitoringStation items
    for i in range(len(stations_within_distance)):
        stations_within_distance[i] = (stations_within_distance[i].name)

    # Order list alphabetically
    stations_within_distance.sort()

    # Print list
    print(stations_within_distance)

if __name__ == "__main__":
    print("\n*** Task 1C: CUED Part IA Flood Warning System ***\n")
    run()

