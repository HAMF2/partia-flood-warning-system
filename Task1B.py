from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()

    # Coordinate to find distance from
    cam_coord = (52.2053, 0.1218)

    # Use stations_by_distance function to find distances and sort
    stations_with_distance = stations_by_distance(stations, cam_coord)

    # Take required data from MonitoringStation items
    for i in range(len(stations_with_distance)):
        stations_with_distance[i] = (stations_with_distance[i][0].name, stations_with_distance[i][0].town, stations_with_distance[i][1])


    # Reduce lists to first 10 and last 10 only
    stations_with_distance_first10 = stations_with_distance[:10]
    stations_with_distance_last10 = stations_with_distance[-10:]

    print(stations_with_distance_first10)
    print(stations_with_distance_last10)

if __name__ == "__main__":
    print("\n*** Task 1B: CUED Part IA Flood Warning System ***\n")
    run()

